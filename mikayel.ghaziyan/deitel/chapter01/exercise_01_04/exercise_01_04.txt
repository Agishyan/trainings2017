Mikayel Ghaziyan
22/09/2017

Exercise 1.4

Categorizing each item as either hardware or software

a.CPU - hardware
b.C++ - software
c.ALU - hardware
d.C++ preprocessor - software
e.Input unit - hardware
f.An editor program - software
