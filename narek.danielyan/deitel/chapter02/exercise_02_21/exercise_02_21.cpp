#include <iostream>

int
main()
{
    std::cout << "*********\t     ***    \t    *  \t         *\n";
    std::cout << "*       *\t   *     *  \t   *** \t        * *\n"; 
    std::cout << "*       *\t  *       * \t  *****\t       *   *\n";
    std::cout << "*       *\t  *       * \t    *  \t      *     *\n";
    std::cout << "*       *\t  *       * \t    *  \t     *       *\n";
    std::cout << "*       *\t  *       * \t    *  \t      *     *\n";
    std::cout << "*       *\t  *       * \t    *  \t       *   *\n";
    std::cout << "*       *\t   *     *  \t    *  \t        * *\n";
    std::cout << "*********\t     ***    \t    *  \t         *\n";
    return 0;
}
