#include <iostream>
#include <cmath>

int
main()
{
    for (int counter = 1; counter <= 5; ++counter) {
        float number;
        std::cout << "Please enter a number (" << counter << " of 5): ";
        std::cin >> number;
        std::cout << "Entered number: " << number << " Floored number: " << std::floor(number + 0.5) << std::endl;
    }

    return 0;
}
