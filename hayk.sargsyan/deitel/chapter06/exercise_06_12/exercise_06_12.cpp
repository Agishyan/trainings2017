#include <iostream>
#include <cmath>

double
calculateCharges(double hours, int car) 
{
    double amount;

    if (hours < 3) {
        amount = 2;
        std::cout << "Car: " << car << " Amount: 2$ Hours: " << hours << std::endl; 
    } else if (hours < 24) {
        amount = 2.0 + ((hours - 3.0) * 0.5);
        std::cout << "Car: " << car << " Amount: " << amount << "$ Hours: " << hours << std::endl;
    } else {
        amount = (std::floor(hours / 24.0) * 10.0) + (fmod(hours, 24.0) * 0.5);
        std::cout << "Car: " << car << " Amount: " << amount << "$ Hours: " << hours << std::endl;
    } 

    return amount;
}

int
main()
{
    double firstCarHours;
    
    std::cout << "Enter the parking hours of the first car: ";
    std::cin >> firstCarHours;
    
    double secondCarHours;
    
    std::cout << "Enter the parking hours of the second car: ";
    std::cin >> secondCarHours;
    
    double thirdCarHours;
    
    std::cout << "Enter the parking hours of the third car: ";
    std::cin >> thirdCarHours;
    
    double amount1 = calculateCharges(firstCarHours, 1);
    double amount2 = calculateCharges(secondCarHours, 2);
    double amount3 = calculateCharges(thirdCarHours, 3);

    std::cout << "Total hours: " << firstCarHours + secondCarHours + thirdCarHours 
              << " Amount: " << amount1 + amount2 + amount3 << "$" << std::endl;

    return 0;
}
