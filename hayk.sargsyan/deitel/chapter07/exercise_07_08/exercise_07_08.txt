A)
std::cout << f[6] << std::endl;

B)
std::cin >> b[4];

C)
for (int counter = 0; counter < 5; ++counter) {
    g[counter] = 8;
}

D)
double sum = 0;

for (int counter = 0; counter < 100; ++counter) {
    sum += g[counter];
}

std::cout << sum << std::endl;

E)
for (int counter = 0; counter < 11; ++counter) {
    b[counter] = a[counter];
}

F)
float max = w[0];
float min = w[0];

for (int counter = 1; counter < 99; ++counter) {
    if (w[counter] > max) {
        max = w[counter];
    }
    if (w[counter] < min) {
        min = w[counter];
    }
}

std::cout << "Maximum value is " << max << std::endl
          << "Minimum value is " << min << std::endl;
