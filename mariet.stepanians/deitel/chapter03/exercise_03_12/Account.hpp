class Account
{
public:
    Account(int balance);
    void setBalance(int balance);
    int getBalance();
    int credit(int creditAmount);
    int debit(int debitAmount);

private:
    int balance_;
};

