Increment can be applied only to one argument.
If programmer tried to add 1 to the sum of x and y, he should initialize a new variable which equals to the sum of x and y (for example, int z = x + y) and then apply increment to it (std::cout << ++z;).
If programmer tried to increment both x and y and then sum them up, he should apply increment to both of them separately: std::cout << (++x) + (++y);
