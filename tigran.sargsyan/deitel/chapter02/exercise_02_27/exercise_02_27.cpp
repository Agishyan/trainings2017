#include <iostream>

int
main()
{
    std::cout << "Cast of A is " << static_cast<int>('A') << std::endl;
    std::cout << "Cast of B is " << static_cast<int>('B') << std::endl;
    std::cout << "Cast of C is " << static_cast<int>('C') << std::endl;
    std::cout << "Cast of a is " << static_cast<int>('a') << std::endl;
    std::cout << "Cast of b is " << static_cast<int>('b') << std::endl;
    std::cout << "Cast of c is " << static_cast<int>('c') << std::endl;
    std::cout << "Cast of 0 is " << static_cast<int>('0') << std::endl;
    std::cout << "Cast of 1 is " << static_cast<int>('1') << std::endl;
    std::cout << "Cast of 2 is " << static_cast<int>('2') << std::endl;
    std::cout << "Cast of $ is " << static_cast<int>('$') << std::endl;
    std::cout << "Cast of * is " << static_cast<int>('*') << std::endl;
    std::cout << "Cast of + is " << static_cast<int>('+') << std::endl;
    std::cout << "Cast of / is " << static_cast<int>('/') << std::endl;
    std::cout << "Cast of space is " << static_cast<int>(' ') << std::endl;

    return 0;
}

