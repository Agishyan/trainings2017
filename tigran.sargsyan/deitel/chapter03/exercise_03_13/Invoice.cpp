#include "Invoice.hpp"

#include <iostream>
#include <string>

Invoice::Invoice(std::string partNumber, std::string description, int quantity, int price)
{
    setPartNumber(partNumber);
    setDescription(description);
    setQuantity(quantity);
    setPrice(price);
}

void
Invoice::setPartNumber(std::string partNumber)
{
    partNumber_ = partNumber;
}

std::string
Invoice::getPartNumber()
{
    return partNumber_;
}

void
Invoice::setDescription(std::string description)
{
    description_ = description;
}

std::string
Invoice::getDescription()
{
    return description_;
}

void
Invoice::setQuantity(int quantity)
{
    if (quantity < 0) {
        std::cout << "Error 1. Quantity is invalid. Resetting to 0." << std::endl;
        quantity_ = 0;
        return;
    }

    quantity_ = quantity;
}

int
Invoice::getQuantity()
{
    return quantity_;
}

void
Invoice::setPrice(int price)
{
    if (price < 0) {
        std::cout << "Error 2. Price is Invalid. Resetting to 0." << std::endl;
        price_ = 0;
        return;
    }

    price_ = price;
}

int
Invoice::getPrice()
{
    return price_;
}

int
Invoice::getInvoiceAmount()
{
    return price_ * quantity_;
}

