#include "Invoice.hpp"

#include <iostream>
#include <string>

int
main()
{
    Invoice invoice1("175aa", "plastic cup", 15, 500);
    std::cout << "Invoice 1:\nPart number: " << invoice1.getPartNumber()
        << "\nDescription: " << invoice1.getDescription()
        << "\nQuantity: " << invoice1.getQuantity()
        << "\nPrice per unit: " << invoice1.getPrice()
        << "\nAmount: " << invoice1.getInvoiceAmount() << std::endl;

    std::string partNumber;
    std::cout << "Enter part number: ";
    getline(std::cin, partNumber);
    invoice1.setPartNumber(partNumber);

    std::string description;
    std::cout << "Enter description: ";
    getline(std::cin, description);
    invoice1.setDescription(description);

    int quantity;
    std::cout << "Enter quantity: ";
    std::cin >> quantity;
    invoice1.setQuantity(quantity);

    int price;
    std::cout << "Enter price: ";
    std::cin >> price;
    invoice1.setPrice(price);

    std::cout << "Invoice 1:\nPart number: " << invoice1.getPartNumber()
        << "\nDescription: " << invoice1.getDescription()
        << "\nQuantity: " << invoice1.getQuantity()
        << "\nPrice per unit: " << invoice1.getPrice()
        << "\nAmount: " << invoice1.getInvoiceAmount() << std::endl;

    return 0;
}

