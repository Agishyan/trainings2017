#include <iostream>
#include <iomanip> 

int
main()
{
    std::cout << "The figures are under each other." << std::endl;
    for (int row = 1; row <= 10; ++row) {
        for (int column = 1; column <= row; ++column) {
            std::cout << "*";
        }
        std::cout << std::endl;
    } 
    std::cout << std::endl;    
    for (int row = 10; row >= 1; --row) {
        for (int column = 1; column <= row; ++column) {
            std::cout << "*";
        } 
        std::cout << std::endl;
    }
    
    std::cout << std::endl;
    for (int row = 1; row <= 10; ++row) {
        for (int column = 2; column <= row; ++column) {
            std::cout << " ";
        }  
        for (int star = (11 - row); star >= 1; --star) {
            std:: cout << "*";
        }         
        std::cout << std::endl;
    }

    std::cout << std::endl;
    for (int row = 10; row >= 1; --row) {
        for (int column = 1; column <= (row - 1); ++column) {
            std::cout << " ";
        }
        for (int star = 10; star >= row; --star) {
            std:: cout << "*";
        }
        std::cout << std::endl;
    }

    std::cout << "\nThe figures are put next to each other." << std::endl;
    for (int row = 1; row <= 10; ++row) {
        for (int column = 1; column <= row; ++column) {
            std::cout << "*";
        } 
        std::cout << std::setw(13 - row);
        for (int column = (11 - row); column >= 1; --column) {
            std::cout << "*";
        } 
        std::cout << std::setw(2 * row);
        for (int column = (11 - row); column >= 1; --column) {
            std::cout << "*";
        }
        std::cout << std::setw(13 - row); 
        for (int column = 1; column <= row; ++column) {
            std::cout << "*";
        }
        std::cout << std::endl;
    }
    return 0;
}

