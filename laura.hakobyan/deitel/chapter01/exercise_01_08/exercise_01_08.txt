
1.8. Distinguish between the terms fatal error and nonfatal error. Why might you prefer to experience a fatal error rather than a nonfatal error?
Division by zero is usually a fatal error. Errors like division by zero occur as a program runs, so they are called runtime errors or execution-time errors.
Nonfatal runtime errors allow programs to run to completion, often producing incorrect results.
A fatal error can be more preferred because it displays an error message and brings immediate termination of the program.

