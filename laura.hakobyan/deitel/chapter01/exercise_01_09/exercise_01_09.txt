a) Why does this text discuss structured programming in addition to object-oriented programming?
Object orientation is a key program methodology, which is used today by programmers. The internal organization of program objects is often built using the methods of structured programming. In addition, the logic of manipulating object is sometimes expressed structurally.

b) What are the typical steps (mentioned in the text) of an object-oriented de sign process?

_analyze a typical requirements document that describes a software system (the ATM) to be built 
_determine the objects required to implement that system
 _determine the attributes the objects will have
_determine the behaviors these objects will exhibit
_specify how the objects interact with one another to meet the system requirements


c) What kinds of messages do people send to one another?
Voice  messages

d) Objects send messages to one another across well-defined interfaces. What interfaces does a car radio (object) present to its user (a person object)?
A person can change channels, increase or decrease the volume.












