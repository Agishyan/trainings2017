Question: (Dangling- Else Problem) State the output for each of the following when x is 9 and y is 11 and when 
x is 11 and y is 9 . Note that the compiler ignores the indentation in a C++ program . The C++ compiler always 
associates an else with the previous if unless told to do otherwise by the placement of braces {} . On first glance, 
the programmer may not be sure which if and else match, so this is referred to as the "dangling- else " problem . We
eliminated the indentation from the following code to make the problem more challenging. [Hint: Apply indentation 
conventions you have learned.]

a. if ( x < 10 )
   if ( y > 10 )
   cout << "*****" << endl;
   else
   cout << "#####" << endl;
   cout << "$$$$$" << endl;

b. if ( x < 10 )
   {
   if ( y > 10 )
   cout << "*****" << endl;
   }
   else
   {
   cout << "#####" << endl;
   cout << "$$$$$" << endl;
   } 

Answer: a.)  x = 9, y = 11. this is output!!!!: *****
                                                $$$$$
             
             x = 11, y = 9. this is output!!!!: $$$$$     
        -----------------------------------------------
        
        b.)  x = 9, y = 11. this is output!!!!: *****
                            
             x = 11, y = 9. this is output!!!!: #####
                                                $$$$$
                            
          
                                        
        
                                        
