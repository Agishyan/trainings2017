#include <iostream>
#include <iomanip>

int
main()
{
    int countOfNumbers = 0;
    int sumOfAll = 0;

    do {
        int eachNumber;
        std::cin >> eachNumber;
        if (eachNumber == 9999) {
            break;
        }
        ++countOfNumbers;
        sumOfAll += eachNumber;
    } while (true);

    std::cout << static_cast<double>(sumOfAll) / countOfNumbers << std::endl;
    return 0;
}
