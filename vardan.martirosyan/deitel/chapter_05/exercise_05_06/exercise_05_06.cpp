#include <iostream>

int
main()
{
    double sum = 0;
    int count = 0;

    do {
        int number;
        std::cout << "Enter number (for the last number write 9999): ";
        std::cin  >> number;

        if (9999 == number) {
            break;
        }
        sum += number;
        ++count;
    } while (true); 

    if (0 == count) {
        std::cerr << "Error 1: first number can not be equal to 9999." << std::endl;
        return 1; 
    }

    std::cout << "Value is " <<(sum / count) << std::endl;
    return 0;
}

