#include <iostream>
#include <iomanip>

int
main()
{
    int number;
    std::cout << "Enter the number: ";
    std::cin  >> number;

    if (number < 1) {
        std::cerr << "Error 1: Number can not be negative." << std::endl;
        return 1;
    }

    for (int horizontal = 1; horizontal <= number; ++horizontal) {
        for (int vertical = 1; vertical <= horizontal; ++vertical) {
            std::cout << "*";
        }
        std::cout << std::endl;
    }
    std::cout << std::endl;

    for (int horizontal = 1; horizontal <= number; ++horizontal) {
        for (int vertical = number + 1 - horizontal; vertical >= 1; --vertical) {
            std::cout << "*";
        }
        std::cout << std::endl;
    }
    std::cout << std::endl;

    for (int horizontal = 1; horizontal <= number; ++horizontal) {
        for (int space = 1; space < horizontal; ++space) {
            std::cout << " ";
        }
        for (int vertical = horizontal; vertical <= number; ++vertical) {            
            std::cout << "*";
        }
        std::cout << std::endl;
    }
    std::cout << std::endl;

    for (int horizontal = 1; horizontal <= number; ++horizontal) {
        for (int space = number; space > horizontal; --space) {
            std::cout << " ";
        }
        for (int vertical = (number + 1 - horizontal); vertical <= number; ++vertical) {
            std::cout << "*";
        }
        std::cout << std::endl;
    }
    std::cout << std::endl;

    for (int line = 1; line <= number; ++line) {
        for (int firstFigure = line; firstFigure > 0; --firstFigure) {
            std::cout << "*";
        }
        std::cout << std::setw(number - line + 2);
        for (int secondFigure = number - line + 1; secondFigure > 0; --secondFigure) {
            std::cout << "*";
        }
        std::cout << std::setw(2 * line);
        for (int thirdFigure = number - line + 1; thirdFigure > 0;  --thirdFigure) {
            std::cout << "*";
        }                    
        std::cout << std::setw(number - line + 2);
        for (int fourthFigure = line; fourthFigure > 0; --fourthFigure) {
            std::cout << "*";
        }
        std::cout << std::endl;
    }
    return 0;
}

