#include <iostream>
#include <cassert>

bool 
even(const int number)
{
    return (0 == number % 2);
}

int
main()
{
    while (true) {
        int number;
        std::cout << "Enter number(-1 to exit): ";
        std::cin  >> number;

        if (-1 == number) {
            break;
        }
        if (even(number)) {
            std::cout << "Number " << number << " is even" << std::endl;
        } else {
            std::cout << "Number " << number << " is odd" << std::endl;
        }
    }

    return 0;
}

